###HULPFUNCTIES###

##POSITIE EN DIMENSIE##
def row(pos):
	"""Geeft de rij terug van de gegeven positie"""
	return pos[0]
assert row([1,2])==1

def col(pos):
	"""Geeft de kolom terug van de gegeven positie"""
	return pos[1]
assert col([1,2])==2

def dim(roster):
	"""Geeft de dimensie van het gegeven rooster.
	De dimensie is gelijk aan het aantal kolommen en rijen"""
	counter=0
	for i in roster:
		counter+=1
	return counter
assert dim([[[1,2],[3,4]],[[5,6],[7,8]]])==2

def pos_ingevuld(roster,pos):
	rij=row(pos)
	kol=col(pos)
	if roster[rij][kol]==0 or roster[rij][kol]==1:
		return True
	else:
		return False

##NULLEN EN EENEN TELLEN##
def nbzero(roster):
	"""Geeft maximale aantal nullen in een rij of kolom van het rooster"""
	dimens=dim(roster)
	return dimens/2.0
assert nbzero([[1,0],[0,0]])==1

def count_zero_row(roster,pos):
	"""Telt het aantal nullen in de rij van de gegeven positie"""
	rij=row(pos)
	dimens=dim(roster)
	counter=0
	for i in range(0,dimens):
		if roster[rij][i]==0:
			counter+=1
	return counter
assert count_zero_row([[1,0],[0,0]],[0,0])==1
assert count_zero_row([[1,0],[0,0]],[0,1])==1
assert count_zero_row([[1,0],[0,0]],[1,0])==2
assert count_zero_row([[1,0],[0,0]],[1,1])==2

def count_one_row(roster,pos):
	"""Telt het aantal eenen in de rij van de gegeven positie"""
	rij=row(pos)
	dimens=dim(roster)
	counter=0
	for i in range(0,dimens):
		if roster[rij][i]==1:
			counter+=1
	return counter
assert count_one_row([[1,0],[1,1]],[0,0])==1
assert count_one_row([[1,0],[1,1]],[0,1])==1
assert count_one_row([[1,0],[1,1]],[1,0])==2
assert count_one_row([[1,0],[1,1]],[1,1])==2

def count_zero_column(roster,pos):
	"""Telt het aantal nullen in de kolom van de gegeven positie"""
	kol=col(pos)
	dimens=dim(roster)
	counter=0
	for i in range(0,dimens):
		if roster[i][kol]==0:
			counter+=1
	return counter
assert count_zero_column([[1,0],[0,0]],[0,0])==1
assert count_zero_column([[1,0],[0,0]],[0,1])==2
assert count_zero_column([[1,0],[0,0]],[1,0])==1
assert count_zero_column([[1,0],[0,0]],[1,1])==2

def count_one_column(roster,pos):
	"""Telt het aantal nullen in de kolom van de gegeven positie"""
	kol=col(pos)
	dimens=dim(roster)
	counter=0
	for i in range(0,dimens):
		if roster[i][kol]==1:
			counter+=1
	return counter
assert count_one_column([[1,1],[0,1]],[0,0])==1
assert count_one_column([[1,1],[0,1]],[0,1])==2
assert count_one_column([[1,1],[0,1]],[1,0])==1
assert count_one_column([[1,1],[0,1]],[1,1])==2


##CHECK_ROSTER NAKIJKEN##
def check_roster(roster):
	"""Controleert of het rooster nog steeds klopt."""
	n=dim(roster)
	for i in range(0,n):
		for j in range(0,n):
			pos=[i,j]
			#We overlopen alle rijen en alle kolommen
			correct_gevuld=find_double(roster,pos)
			#dit getal zou in het rooster moeten staan indien het correct gevuld is
			if correct_gevuld != None and pos_ingevuld(roster,pos)==True:
				#er is wel degelijk een paar naast deze positie en deze positie is ingevuld
				if roster[i][j]!=correct_gevuld:
					return False
	return True

def filled_correct_roster(roster):
	"""Controleert of het rooster volledig correct gevuld is"""
	n=dim(roster)
	for i in range(0,n):
		for j in range(0,n):
			if roster[i][j]!=0 and roster[i][j]!=1:
				return False

	if check_roster(roster)==False:
		return False
	else:
		return True

##HOOFDFUNCTIES##
def two_neighbours(roster,pos):
	"""Geeft een lijst van tuples. Deze tuppels bevatten telkens de getallen die ingevuld zijn in de vakjes van de buren.
	Deze lijst kan gebruikt worden om meteen te zien of er een dubbel paar is."""
	rij=row(pos)
	kol=col(pos)
	n=dim(roster)
	result=[]

	if rij==0 or rij==1:
		#we zitten op de eerste of tweede rij
		#nog geen getallen boven!
		if kol==0 or kol==1:
			#we zitten op de eerste of tweede kolom
			#nog geen getallen links!
			number_one=roster[rij][kol+1]
			number_two=roster[rij][kol+2]
			result.append((number_one,number_two))
			#de twee getallen rechts zijn toegevoegd

			number_three=roster[rij+1][kol]
			number_four=roster[rij+2][kol]
			result.append((number_three,number_four))
			#de twee getallen onder zijn toegevoegd

		elif kol==n-1 or kol==n-2:
			#we zitten op de twee laatste kolommen
			#nog geen getallen rechts!
			number_one=roster[rij][kol-1]
			number_two=roster[rij][kol-2]
			result.append((number_one,number_two))
			#de twee getallen links zijn toegevoegd

			number_three=roster[rij+1][kol]
			number_four=roster[rij+2][kol]
			result.append((number_three,number_four))
			#de twee getallen onder zijn toegevoegd

		else:
			#zowel getallen links als rechts (en onder)
			number_one=roster[rij][kol-1]
			number_two=roster[rij][kol-2]
			result.append((number_one,number_two))
			#de getallen links zijn teogevoegd

			number_three=roster[rij+1][kol]
			number_four=roster[rij+2][kol]
			result.append((number_three,number_four))
			#de twee getallen onder zijn toegevoegd

			number_five=roster[rij][kol+1]
			number_six=roster[rij][kol+2]
			result.append((number_five,number_six))
			#de twee getallen rechts zijn toegevoegd

	elif rij==n-1 or rij==n-2:
		#we zitten op een van de twee onderste rijen
		#geen getallen onder!

		if kol==0 or kol==1:
			#we zitten op de eerste of tweede kolom
			#nog geen getallen links!
			number_one=roster[rij][kol+1]
			number_two=roster[rij][kol+2]
			result.append((number_one,number_two))
			#de twee getallen rechts zijn toegevoegd

			number_three=roster[rij-1][kol]
			number_four=roster[rij-2][kol]
			result.append((number_three,number_four))
			#de twee getallen boven zijn toegevoegd

		elif kol==n-1 or kol==n-2:
			#we zitten op de twee laatste kolommen
			#nog geen getallen rechts!
			number_one=roster[rij][kol-1]
			number_two=roster[rij][kol-2]
			result.append((number_one,number_two))
			#de twee getallen links zijn toegevoegd

			number_three=roster[rij-1][kol]
			number_four=roster[rij-2][kol]
			result.append((number_three,number_four))
			#de twee getallen boven zijn toegevoegd

		else:
			#zowel getallen links als rechts (en boven)
			number_one=roster[rij][kol-1]
			number_two=roster[rij][kol-2]
			result.append((number_one,number_two))
			#de getallen links zijn teogevoegd

			number_three=roster[rij-1][kol]
			number_four=roster[rij-2][kol]
			result.append((number_three,number_four))
			#de twee getallen boven zijn toegevoegd

			number_five=roster[rij][kol+1]
			number_six=roster[rij][kol+2]
			result.append((number_five,number_six))
			#de twee getallen rechts zijn teogevoegd

	else:
		#zowel boven als onder getallen
		if kol==0 or kol==1:
			#we zitten op de eerste of tweede kolom
			#nog geen getallen links!
			number_one=roster[rij][kol+1]
			number_two=roster[rij][kol+2]
			result.append((number_one,number_two))
			#de twee getallen rechts zijn toegevoegd

			number_three=roster[rij-1][kol]
			number_four=roster[rij-2][kol]
			result.append((number_three,number_four))
			#de twee getallen boven zijn toegevoegd

			number_five=roster[rij+1][kol]
			number_six=roster[rij+2][kol]
			result.append((number_five,number_six))
			#de twee getallen onder zijn toegevoegd

		elif kol==n-1 or kol==n-2:
			#we zitten op de twee laatste kolommen
			#nog geen getallen rechts!
			number_one=roster[rij][kol-1]
			number_two=roster[rij][kol-2]
			result.append((number_one,number_two))
			#de twee getallen links zijn toegevoegd

			number_three=roster[rij-1][kol]
			number_four=roster[rij-2][kol]
			result.append((number_three,number_four))
			#de twee getallen boven zijn toegevoegd

			number_five=roster[rij+1][kol]
			number_six=roster[rij+2][kol]
			result.append((number_five,number_six))
			#de twee getallen onder zijn toegevoegd

		else:
		#zowel getallen links als rechts (en boven en onder)
			number_one=roster[rij][kol-1]
			number_two=roster[rij][kol-2]
			result.append((number_one,number_two))
			#de getallen links zijn teogevoegd

			number_three=roster[rij-1][kol]
			number_four=roster[rij-2][kol]
			result.append((number_three,number_four))
			#de twee getallen boven zijn toegevoegd

			number_five=roster[rij][kol+1]
			number_six=roster[rij][kol+2]
			result.append((number_five,number_six))
			#de twee getallen rechts zijn teogevoegd

			number_seven=roster[rij+1][kol]
			number_eight=roster[rij+2][kol]
			result.append((number_seven,number_eight))
			#de twee getallen onder zijn toegevoegd

	return result

def find_double(roster,pos):
	"""Kijkt of er al een dubbel paar is bij de buren. Indien dit het geval is, geeft de functie de andere waarde terug.
	Indien dit niet het geval is, geeft de functie niets terug."""
	lst=two_neighbours(roster,pos)

	for i in lst:
		if i[0]==i[1]:
			if i[0]==0:
				return 1
			elif i[0]==1:
				return 0

def fill_row(roster,pos,value):
	"""Vult de lege plaatsen van het rooster op de rij van de gegeven positie met de gegeven waarde"""
	rij=row(pos)
	kol=col(pos)
	n=dim(roster)
	for i in range(0,n):
		#Alle kolommen worden overlopen
		pos_inv=(rij,i)
		if pos_ingevuld(roster,pos_inv)==False:
			roster[rij][i]=value

def fill_col(roster,pos,value):
	"""Vult de lege plaatsen van het rooster op de kolom van de gegeven positie met de gegeven waarde"""
	rij=row(pos)
	kol=col(pos)
	n=dim(roster)
	for i in range(0,n):
		#Alle rijen worden overlopen
		pos_inv=(i,kol)
		if pos_ingevuld(roster,pos_ingevuld)==False:
			roster[i][kol]=value

def fill_roster(roster,pos=(0,0)):
	rij=row(pos)
	kol=col(pos)
	n=dim(roster)
	max_zero=nbzero(roster)

	if filled_correct_roster(roster)==True:
		return True

	if count_zero_column==max_zero:
		fill_col(roster,pos,1)
	elif count_one_column==max_zero:
		fill_col(roster,pos,0)

	if count_zero_row==max_zero:
		fill_col(roster,pos,1)